package handlers

import (
	"article/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateArticle godoc
// @tags article
// @ID create-article-handler
// @Summary Create Article
// @Description Create Article By Given Info and Author ID
// @Param data body models.ArticleCreateModel true "Article Body"
// @Accept  json
// @Produce  json
// @Success 200 {object} models.SuccessResponse{data=string}
// @Failure default {object} models.DefaultError
// @Router /articles [POST]
func (h *Handler) CreateArticle(c *gin.Context) {
	var entity models.ArticleCreateModel
	err := c.BindJSON(&entity)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
		return
	}

	err = h.strg.Article().Create(entity)

	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	c.JSON(200, models.SuccessResponse{
		Message: "article has been created",
		Data:    "ok",
	})
}

// GetArticleList godoc
// @tags article
// @ID get-all-handler
// @Summary List articles
// @Description get all articles
// @Param offset query int false "offset"
// @Param limit query int false "limit"
// @Param search query string false "search string"
// @Accept  json
// @Produce  json
// @Success 200 {array} models.ArticleListItem
// @Failure default {object} models.DefaultError
// @Router /articles [get]
func (h *Handler) GetArticleList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	resp, err := h.strg.Article().GetList(models.Query{Offset: offset, Limit: limit, Search: c.Query("search")})

	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	c.JSON(200, resp)
}

// GetByIDHandler godoc
// @Router /articles/{id} [GET]
// @tags article
// @Summary Get one article by ID
// @Description Used to get article using its ID
// @ID Get-Article-By-ID-Handler
// @Param id path int true "Article ID"
// @Success 200  {object} models.SuccessResponse
// @Failure 400,404 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) GetArticleByID(c *gin.Context) {
	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	resp, err := h.strg.Article().GetByID(id)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
	}
	c.JSON(200, models.SuccessResponse{
		Message: "ok",
		Data:    resp,
	})
}

// UpdateHandler godoc
// @Router /articles/{id} [PUT]
// @tags article
// @Summary Update particular article
// @Description Used to update any article related to specified ID in params.
// @ID Article-Update-Handler
// @Param id path int true "Article ID"
// @Param data body models.ArticleUpdateModel true "Updating data"
// @Success 200  {object} models.SuccessResponse
// @Failure 400,404 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) ArticleUpdate(c *gin.Context) {
	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	var artcl models.ArticleUpdateModel
	err = c.BindJSON(&artcl)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	if artcl.ID != id {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	resp, err := h.strg.Article().Update(artcl)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	c.JSON(200, models.SuccessResponse{
		Message: "Updated successfully",
		Data:    resp,
	})
}

// DeleteHandler godoc
// @Router /articles/{id} [DELETE]
// @tags article
// @Summary Delete an article
// @Description Used to delete articles using it's ID
// @ID Author-Delete-Handler
// @Param id path int true "Article ID"
// @Success 200  {object} models.SuccessResponse
// @Failure 400,404 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) ArticleDelete(c *gin.Context) {
	str := c.Param("id")
	id, err := strconv.Atoi(str)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	resp, err := h.strg.Article().Delete(id)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	c.JSON(200, models.SuccessResponse{
		Message: "Deleted successfully",
		Data:    resp,
	})

}
