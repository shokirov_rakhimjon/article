package handlers

import (
	"article/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateAuthor godoc
// @tags author
// @ID create-author-handler
// @Summary Create Author
// @Description Create Author By Given Info and Author ID
// @Param data body models.PersonCreateModel true "Author Body"
// @Accept  json
// @Produce  json
// @Success 200 {object} models.SuccessResponse{data=string}
// @Failure default {object} models.DefaultError
// @Router /author [POST]
func (h *Handler) CreateAuthor(c *gin.Context) {
	var entity models.PersonCreateModel
	err := c.BindJSON(&entity)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
		return
	}

	err = h.strg.Author().Create(entity)

	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	c.JSON(200, models.SuccessResponse{
		Message: "article has been created",
		Data:    "ok",
	})
}

// GetAuthorList godoc
// @tags author
// @ID get-all-author-handler
// @Summary List author
// @Description get all author
// @Param offset query int false "offset"
// @Param limit query int false "limit"
// @Param search query string false "search string"
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Person
// @Failure default {object} models.DefaultError
// @Router /author [GET]
func (h *Handler) GetAuthorList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	resp, err := h.strg.Author().GetList(models.Query{Offset: offset, Limit: limit, Search: c.Query("search")})

	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	c.JSON(200, resp)
}

// GetByIDHandler godoc
// @Router /author/{id} [GET]
// @tags author
// @Summary Get one author by ID
// @Description Used to get author using its ID
// @ID Get-Author-By-ID-Handler
// @Param id path int true "Author ID"
// @Success 200  {object} models.SuccessResponse
// @Failure 400,404 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) GetAuthorByID(c *gin.Context) {
	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	resp, err := h.strg.Author().GetByID(id)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
	}
	c.JSON(200, models.SuccessResponse{
		Message: "ok",
		Data:    resp,
	})
}

// UpdateHandler godoc
// @Router /author/{id} [PUT]
// @tags author
// @Summary Update particular author
// @Description Used to update any author related to specified ID in params.
// @ID Author-Update-Handler
// @Param id path int true "Author ID"
// @Param data body models.PersonUpdateModel true "Updating data"
// @Success 200  {object} models.SuccessResponse
// @Failure 400,404 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) AuthorUpdate(c *gin.Context) {
	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	var auth models.PersonUpdateModel
	err = c.BindJSON(&auth)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	if auth.ID != id {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	resp, err := h.strg.Author().Update(auth)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	c.JSON(200, models.SuccessResponse{
		Message: "Updated successfully",
		Data:    resp,
	})
}

// DeleteHandler godoc
// @Router /author/{id} [DELETE]
// @tags author
// @Summary Delete an author
// @Description Used to delete author using it's ID
// @ID Author-Delete-Handler
// @Param id path int true "Author ID"
// @Success 200  {object} models.SuccessResponse
// @Failure 400,404 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) AuthorDelete(c *gin.Context) {
	str := c.Param("id")
	id, err := strconv.Atoi(str)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	resp, err := h.strg.Author().Delete(id)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	c.JSON(200, models.SuccessResponse{
		Message: "Deleted successfully",
		Data:    resp,
	})

}
