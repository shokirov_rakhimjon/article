package api

import (
	"article/api/docs"
	_ "article/api/docs"
	"article/api/handlers"
	"article/config"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// @description This is a sample article demo.
// @termsOfService https://udevs.io
func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.App
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Host = cfg.ServiceHost + cfg.HTTPPort
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	r.POST("/articles", h.CreateArticle)
	r.GET("/articles", h.GetArticleList)
	r.GET("/articles/:id", h.GetArticleByID)
	r.PUT("/articles/:id", h.ArticleUpdate)
	r.DELETE("/articles/:id", h.ArticleDelete)

	r.POST("/author", h.CreateAuthor)
	r.GET("/author", h.GetAuthorList)
	r.GET("/author/:id", h.GetAuthorByID)
	r.PUT("/author/:id", h.AuthorUpdate)
	r.DELETE("/author/:id", h.AuthorDelete)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
