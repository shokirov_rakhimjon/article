package postgres

import (
	"article/models"
	"article/storage"
	"database/sql"

	"github.com/jmoiron/sqlx"
)

type authorRepo struct {
	db *sqlx.DB
}

func NewAuthorRepo(db *sqlx.DB) storage.AuthorRepoI {
	return authorRepo{
		db: db,
	}
}

func (r authorRepo) Create(entity models.PersonCreateModel) (err error) {
	insertQuery := `INSERT INTO author (
		firstname,
		lastname
	) VALUES (
		$1,
		$2
	)`

	_, err = r.db.Exec(insertQuery,
		entity.Firstname,
		entity.Lastname,
	)

	return err
}

func (r authorRepo) GetList(query models.Query) (resp []models.Person, err error) {
	var rows *sql.Rows
	if len(query.Search) > 0 {
		rows, err = r.db.Query(
			`SELECT * FROM author
			WHERE firstname ILIKE '%' || $3 || '%' or lastname ILIKE '%' || $3 || '%'
			OFFSET $1 LIMIT $2`,
			query.Offset,
			query.Limit,
			query.Search,
		)
	} else {
		rows, err = r.db.Query(
			`SELECT * FROM author
			OFFSET $1 LIMIT $2`,
			query.Offset,
			query.Limit,
		)
	}

	if err != nil {
		return resp, err
	}

	defer rows.Close()
	for rows.Next() {
		var a models.Person
		err = rows.Scan(
			&a.ID, &a.Firstname, &a.Lastname, &a.CreatedAt, &a.UpdatedAt,
		)
		resp = append(resp, a)
		if err != nil {
			return resp, err
		}
	}

	return resp, err
}

func (r authorRepo) GetByID(ID int) (resp []models.Person, err error) {
	var rows1 *sql.Rows
	rows1, err = r.db.Query(
		`SELECT * FROM author 
		WHERE author.id= $1`,
		int(ID),
	)
	if err != nil {
		return resp, err
	}
	defer rows1.Close()
	for rows1.Next() {
		var a models.Person
		err = rows1.Scan(&a.ID, &a.Firstname, &a.Lastname, &a.CreatedAt, &a.UpdatedAt)
		resp = append(resp, a)
		if err != nil {
			return resp, err
		}
	}
	return
}

func (r authorRepo) Update(entity models.PersonUpdateModel) (effectedRowsNum int, err error) {
	query := `UPDATE author SET
		firstname = :firstname,
		lastname = :lastname,
		updated_at = now()
		WHERE
		id = :id`

	params := map[string]interface{}{
		"id":        entity.ID,
		"firstname": entity.Firstname,
		"lastname":  entity.Lastname,
	}

	result, err := r.db.NamedExec(query, params)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return int(rowsAffected), err
}

func (r authorRepo) Delete(ID int) (effectedRowsNum int, err error) {
	res3, err := r.db.Exec(
		`DELETE FROM author
		 WHERE id = $1`,
		ID,
	)
	if err != nil {
		return 0, err
	}
	nums, err := res3.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(nums), err
}
