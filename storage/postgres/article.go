package postgres

import (
	"article/models"
	"article/storage"
	"database/sql"

	"github.com/jmoiron/sqlx"
)

type articleRepo struct {
	db *sqlx.DB
}

func NewArticleRepo(db *sqlx.DB) storage.ArticleRepoI {
	return articleRepo{
		db: db,
	}
}

func (r articleRepo) Create(entity models.ArticleCreateModel) (err error) {
	insertQuery := `INSERT INTO article (
		title,
		body,
		author_id
	) VALUES (
		$1,
		$2,
		$3
	)`

	_, err = r.db.Exec(insertQuery,
		entity.Title,
		entity.Body,
		entity.AuthorID,
	)

	return err
}

func (r articleRepo) GetList(query models.Query) (resp []models.ArticleListItem, err error) {
	var rows *sql.Rows
	if len(query.Search) > 0 {
		rows, err = r.db.Query(
			`SELECT
			ar.id, ar.title, ar.body, ar.created_at, ar.updated_at,
			au.id, au.firstname, au.lastname, au.created_at, au.updated_at
			FROM article AS ar JOIN author AS au ON ar.author_id = au.id
			WHERE title ILIKE '%' || $3 || '%'
			OFFSET $1 LIMIT $2`,
			query.Offset,
			query.Limit,
			query.Search,
		)
	} else {
		rows, err = r.db.Query(
			`SELECT
			ar.id, ar.title, ar.body, ar.created_at, ar.updated_at,
			au.id, au.firstname, au.lastname, au.created_at, au.updated_at
			FROM article AS ar JOIN author AS au ON ar.author_id = au.id
			OFFSET $1 LIMIT $2`,
			query.Offset,
			query.Limit,
		)
	}

	if err != nil {
		return resp, err
	}

	defer rows.Close()
	for rows.Next() {
		var a models.ArticleListItem
		err = rows.Scan(
			&a.ID, &a.Title, &a.Body, &a.CreatedAt, &a.UpdatedAt,
			&a.Author.ID, &a.Author.Firstname, &a.Author.Lastname, &a.Author.CreatedAt, &a.Author.UpdatedAt,
		)
		resp = append(resp, a)
		if err != nil {
			return resp, err
		}
	}

	return resp, err
}

func (r articleRepo) GetByID(ID int) (resp []models.Article, err error) {
	var rows1 *sql.Rows
	rows1, err = r.db.Query(
		`SELECT article.id, article.title, article.body, article.author_id, article.created_at, article.updated_at
		FROM article 
		WHERE article.id= $1`,
		int(ID),
	)
	if err != nil {
		return resp, err
	}
	defer rows1.Close()
	for rows1.Next() {
		var a models.Article
		err = rows1.Scan(&a.ID, &a.Title, &a.Body, &a.AuthorID, &a.CreatedAt, &a.UpdatedAt)
		resp = append(resp, a)
		if err != nil {
			return resp, err
		}
	}
	return
}

func (r articleRepo) Update(entity models.ArticleUpdateModel) (effectedRowsNum int, err error) {
	query := `UPDATE article SET
		title = :title,
		body = :body,
		updated_at = now()
		WHERE
		id = :id`

	params := map[string]interface{}{
		"body":  entity.Body,
		"title": entity.Title,
		"id":    entity.ID,
	}

	result, err := r.db.NamedExec(query, params)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return int(rowsAffected), err
}

func (r articleRepo) Delete(ID int) (effectedRowsNum int, err error) {
	res3, err := r.db.Exec(
		`DELETE FROM article WHERE id = $1`,
		ID,
	)
	if err != nil {
		return 0, err
	}

	nums, err := res3.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(nums), err
}
